package com.wolfsoft.androiddevelopmentreusablecomponents.models;

/**
 * Created by foram on 28/2/17.
 */
public class User {
    private  String token;

    private UserObj user;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserObj getObject() {
        return user;
    }

    public void setObject(UserObj object) {
        this.user = object;
    }

    public class UserObj {
        private String name;

        private String email;

        private String mobile;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }
    }
}



