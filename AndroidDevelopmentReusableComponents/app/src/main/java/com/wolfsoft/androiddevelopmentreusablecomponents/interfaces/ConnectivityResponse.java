package com.wolfsoft.androiddevelopmentreusablecomponents.interfaces;

/**
 * Created by foram on 18/5/17.
 */

public interface ConnectivityResponse {
    void processFinish(Boolean isConnected);
}
