package com.wolfsoft.androiddevelopmentreusablecomponents.globals;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import com.wolfsoft.androiddevelopmentreusablecomponents.interfaces.ConnectivityResponse;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by foram on 18/5/17.
 */

public class ConnectionDetector extends AsyncTask<String, Void, Boolean> {
    public ConnectivityResponse delegate = null;

    Context context;
    public ConnectionDetector(Context context,ConnectivityResponse response) {
        this.context=context;
        delegate=response;
    }

    @Override
    protected Boolean doInBackground(String... strings) {
        if (networkConnectivity()) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL(
                        "http://www.google.com").openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(3000);
                urlc.setReadTimeout(4000);
                urlc.connect();
                return (urlc.getResponseCode() == 200);
            } catch (IOException e) {
                return (false);
            }
        } else
            return false;
    }

    @Override
    protected void onPostExecute(Boolean isConnected) {
        delegate.processFinish(isConnected);
    }

    private boolean networkConnectivity() {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }
}
