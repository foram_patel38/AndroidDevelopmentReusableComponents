package com.wolfsoft.androiddevelopmentreusablecomponents.interfaces;


import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Header;

public interface RetrofitServiceInterface {


    String url = "http://greatgroup.carekaro.com/app/public";


    @GET("/user/states")
    void getStates(Callback<Response> cb);

}
