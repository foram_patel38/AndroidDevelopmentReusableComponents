package com.wolfsoft.androiddevelopmentreusablecomponents.globals;

import android.content.Context;

import com.wolfsoft.androiddevelopmentreusablecomponents.helpers.AppHelper;
import com.wolfsoft.androiddevelopmentreusablecomponents.models.User;


/**
 * Created by one on 31/10/15.
 */
public class ComplexPrefUtils {

    // Set and get user
    public static void setuser(User user, Context context){
        AppHelper.logE("set user ","called");
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "user", 0);;
        complexPreferences.putObject("user", user);
        complexPreferences.commit();
    }

    public static User getUser(Context context){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "user", 0);
        if(complexPreferences != null)
        {
            User user = complexPreferences.getObject("user", User.class);
//            AppHelper.logE("***user obj",user+"");
            if(user != null){
//                if(user.getObject() != null)
//                    AppHelper.logE("***user obj Mobile",user.getObject().getMobile()+"");
                AppHelper.logE("token",user.getToken()+"");
                if(user.getToken() != null)
                    return user;
                else
                    return null;
            }
            else
                return null;
        }
        else
            return null;
    }

    public static void clearUser(Context context){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(context, "user", 0);
        complexPreferences.clearObject();
        complexPreferences.commit();
    }


}
