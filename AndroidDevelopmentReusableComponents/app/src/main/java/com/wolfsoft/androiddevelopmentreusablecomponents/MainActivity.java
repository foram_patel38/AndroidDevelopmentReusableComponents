package com.wolfsoft.androiddevelopmentreusablecomponents;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import com.wolfsoft.androiddevelopmentreusablecomponents.helpers.RestClient;
import com.wolfsoft.androiddevelopmentreusablecomponents.interfaces.RetrofitServiceInterface;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btnTest)
    Button btnTest;
    RetrofitServiceInterface retrofitServiceInterface;
    RestClient client;
    Context context=MainActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        client=new RestClient(context);
        retrofitServiceInterface=client.setUpInterface();

    }

    @OnClick(R.id.btnTest)
    public void ButtonClick() {
        loadData();
    }

    void loadData(){
        // if you want to show custom message for dialog
        //client.showProgressDialog("Your message");

        // for default message
        client.showProgressDialog();
        retrofitServiceInterface.getStates(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                JSONObject object= client.getObjectFromResponse(response);
            }

            @Override
            public void failure(RetrofitError error) {
                client.showErrorMessage(error);
            }
        });
    }
}
