package com.wolfsoft.androiddevelopmentreusablecomponents.helpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.wolfsoft.androiddevelopmentreusablecomponents.interfaces.RetrofitServiceInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by foram on 29/5/17.
 */

public class RestClient {
    Context context;
    ProgressDialog loading;
    RestAdapter adapter;

    public RestClient(Context context) {
        this.context = context;
        adapter = new RestAdapter.Builder().setEndpoint(RetrofitServiceInterface.url).build();
    }

    public void showProgressDialog(String progressMsg) {
        loading = new ProgressDialog(context);
        loading.setMessage(progressMsg);
        loading.show();
        loading.setCancelable(false);
    }

    public void showProgressDialog() {
        loading = new ProgressDialog(context);
        loading.setMessage("Please Wait..");
        loading.show();
        loading.setCancelable(false);
    }

    public RetrofitServiceInterface setUpInterface() {
        return adapter.create(RetrofitServiceInterface.class);
    }

    public JSONObject getObjectFromResponse(Response response) {
        JSONObject object = null;
        if (loading != null)
            loading.dismiss();
        String result = new String(((TypedByteArray) response.getBody()).getBytes());
        AppHelper.logE("RESPONSE", result);
        try {
            object = new JSONObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

    public void showErrorMessage(RetrofitError error) {
        if (loading != null)
            loading.dismiss();
        AppHelper.logE("Error", error + "");
        if (error.getKind().equals(RetrofitError.Kind.NETWORK)) {
            Toast.makeText(context, "No internet connection", Toast.LENGTH_LONG).show();
        } else {
            if (error.getResponse() != null) {
                AppHelper.logE("errorStatus", error.getResponse().getStatus() + "");
                AppHelper.logE("errorResponse", error.getResponse().toString());
                AppHelper.logE("Error", new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                if (error.getResponse().getStatus() == 422) {
                    JSONObject mainErrorObj = null;
                    try {
                        mainErrorObj = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        JSONObject errorKeyObj = mainErrorObj.getJSONObject("errors");
                        Iterator<String> iter = errorKeyObj.keys();
                        while (iter.hasNext()) {
                            String key = iter.next();

                            Object value = errorKeyObj.get(key);
                            String errMsg = "";
                            AppHelper.logE("Error Values", value.toString());
                            if (value instanceof JSONArray) {
                                JSONArray array = new JSONArray(value.toString());
                                errMsg = array.get(0).toString();
                            }

                            Toast.makeText(context, errMsg, Toast.LENGTH_SHORT).show();
                            break;

                        }
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    JSONObject mainErrorObj = null;
                    try {
                        mainErrorObj = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        String msg = mainErrorObj.getString("message");
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }

    }


}
