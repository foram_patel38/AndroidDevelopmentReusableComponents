package com.wolfsoft.androiddevelopmentreusablecomponents.helpers;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wolfsoft.androiddevelopmentreusablecomponents.globals.ComplexPrefUtils;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * Created by foram on 28/2/17.
 */

public class AppHelper {

    private static final boolean DEBUG_MODE = true;
    private static final String TAG = "[RIHAAPP]";

    static ProgressDialog loading;


    public static String getUserToken(Context context) {
        return ComplexPrefUtils.getUser(context).getToken();
    }

    public static void logE(String key, String value) {
        if (DEBUG_MODE) {
            if (key != null)
                Log.e(key, value);
            else
                Log.e(TAG, value);
        }
    }

    public static void logE(String value) {
        if (DEBUG_MODE) {
            Log.e(TAG, value);
        }
    }

    public static <T> T gson_jsonToClass(String object, Class<T> classToConvert){
        return new Gson().fromJson(object,classToConvert);
    }

    public static String gson_classToJson(Object classToConvert){
        return new Gson().toJson(classToConvert);
    }

    public static void showLongToast(Context context,String message){
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }

    public static void showShortToast(Context context,String message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }

    public static String toBase64(Bitmap bitmap){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }


}
